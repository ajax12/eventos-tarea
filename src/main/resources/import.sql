INSERT INTO eventos (titulo,descripcion,ponente,fecha_inicio,fecha_cierre,imagen,activo) VALUES( 'Privacidad y seguridad','Ciberseguridad y delitos informaticoa', 'Alfredo Castillo', '2021/09/19','2021/09/21','delitos.png',true);
INSERT INTO eventos (titulo,descripcion,ponente,fecha_inicio,fecha_cierre,imagen,activo) VALUES( 'Aplicaciones reactivas','Ciberseguridad y delitos informaticoa', 'Alfredo Castillo', '2021/09/19','2021/09/21','rx.png',false);
INSERT INTO eventos (titulo,descripcion,ponente,fecha_inicio,fecha_cierre,imagen,activo) VALUES( 'Aplicaciones seguras','Ciberseguridad y delitos informaticoa', 'Alfredo Castillo', '2021/09/19','2021/09/21','security.png',false);
INSERT INTO eventos (titulo,descripcion,ponente,fecha_inicio,fecha_cierre,imagen,activo) VALUES( 'Swift para empezar','Ciberseguridad y delitos informaticoa', 'Alfredo Castillo', '2021/09/19','2021/09/21','swift.png',true);


INSERT INTO talleres(inicio,termino,nombre_taller,descripcion_taller,impartido,costo_taller) VALUES ('2021/09/19','2021/09/19','MICROSERVICIOS NIVEL 1',"MR,EUREKA Y MAS...",'ANA LARA',300.00);
INSERT INTO talleres(inicio,termino,nombre_taller,descripcion_taller,impartido,costo_taller) VALUES ('2021/09/19','2021/09/19','MICROSERVICIOS NIVEL 2',"MRICOSERVICIOS,EUREKA Y MAS...",'JULIO TOLEDO',300.00);
INSERT INTO talleres(inicio,termino,nombre_taller,descripcion_taller,impartido,costo_taller) VALUES ('2021/09/19','2021/09/19','MICROSERVICIOS NIVEL 3',"MRICOSERVICIOS,EUREKA Y MAS...",'PEDRO SOTO',300.00);
INSERT INTO talleres(inicio,termino,nombre_taller,descripcion_taller,impartido,costo_taller) VALUES ('2021/09/19','2021/09/19','AJAX Y JS',"SOLO PARA CONOCEDORES",'JUAN GARCIA',300.00);
INSERT INTO talleres(inicio,termino,nombre_taller,descripcion_taller,impartido,costo_taller) VALUES ('2021/09/19','2021/09/19','ANGULAR PRINCIPIANTES',"ESTRUCTURA DE PROYECTO Y MODULOS",'ANDRES VARGAS',300.00);
INSERT INTO talleres(inicio,termino,nombre_taller,descripcion_taller,impartido,costo_taller) VALUES ('2021/09/19','2021/09/19','PYTHON TOTAL',"PANDAS, COCOS 2D, BIG DATA",'SOFIA HANK',300.00);

INSERT INTO opciones(comida,costo_comida,costo_evento,costo_taller,fecha_de_compra,total,taller_id) VALUES (1,200,800,500,'2021/10/20',1500,1);
INSERT INTO opciones(comida,costo_comida,costo_evento,costo_taller,fecha_de_compra,total,taller_id) VALUES (1,200,800,500,'2021/10/20',1500,2);
INSERT INTO opciones(comida,costo_comida,costo_evento,costo_taller,fecha_de_compra,total,taller_id) VALUES (1,200,800,500,'2021/10/20',1500,3);
INSERT INTO opciones(comida,costo_comida,costo_evento,costo_taller,fecha_de_compra,total,taller_id) VALUES (1,200,800,500,'2021/10/20',1500,4);
INSERT INTO opciones(comida,costo_comida,costo_evento,costo_taller,fecha_de_compra,total,taller_id) VALUES (0,0,500,300,'2021/07/20',800,5);
INSERT INTO opciones(comida,costo_comida,costo_evento,costo_taller,fecha_de_compra,total,taller_id) VALUES (1,200,500,300,'2021/06/20',1000,6);
INSERT INTO opciones(comida,costo_comida,costo_evento,costo_taller,fecha_de_compra,total,taller_id) VALUES (0,0,500,300,'2021/08/20',800,1);
INSERT INTO opciones(comida,costo_comida,costo_evento,costo_taller,fecha_de_compra,total,taller_id) VALUES (1,200,500,300,'2021/10/20',1000,1);
INSERT INTO opciones(comida,costo_comida,costo_evento,costo_taller,fecha_de_compra,total,taller_id) VALUES (1,200,500,300,'2021/10/20',1000,1);

INSERT INTO asistentes(nombre,direccion,telefono,evento_id,opcion_id) VALUES ('carlos ramirez', "cdmx col Presidentes calle cielo sin numero","55-12-32-34-44",1,1); 
INSERT INTO asistentes(nombre,direccion,telefono,evento_id,opcion_id) VALUES ('pepe garcia', "cdmx col Lomas de San lorenzo calle naranjos, numero 12","55-12-22-34-43",2,2); 
INSERT INTO asistentes(nombre,direccion,telefono,evento_id,opcion_id) VALUES ('ana tores', "cdmx col la presa calle cama de piedra, numero 99","00-13-13-00-00",3,3); 
INSERT INTO asistentes(nombre,direccion,telefono,evento_id,opcion_id) VALUES ('Luis uno', "cdmx col montes calle de tierra sin numero","55-12-42-34-00",4,4); 
INSERT INTO asistentes(nombre,direccion,telefono,evento_id,opcion_id) VALUES ('genaro dos', "cdmx col mixe calle frutas, numero 74","55-18-22-34-93",1,5); 
INSERT INTO asistentes(nombre,direccion,telefono,evento_id,opcion_id) VALUES ('lupe tres', "cdmx col rio calle estrella, numero 23","00-13-13-99-00",1,6); 
INSERT INTO asistentes(nombre,direccion,telefono,evento_id,opcion_id) VALUES ('manuel cuatro', "cdmx col sur calle agua sin numero","55-12-32-34-54",2,7); 
INSERT INTO asistentes(nombre,direccion,telefono,evento_id,opcion_id) VALUES ('pepe cinco', "cdmx col lili calle tuna, numero 14","55-12-22-34-13",2,8); 
INSERT INTO asistentes(nombre,direccion,telefono,evento_id,opcion_id) VALUES ('ana seis', "cdmx col las rosas , numero 68","00-13-13-10-11",2,9); 



























