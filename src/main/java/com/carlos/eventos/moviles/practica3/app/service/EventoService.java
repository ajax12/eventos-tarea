package com.carlos.eventos.moviles.practica3.app.service;

import java.util.List;

import com.carlos.eventos.moviles.practica3.app.entity.Evento;

public interface EventoService {
	
	public List<Evento> listarEventos();
	
	public Evento buscarEventoPorId(Long id);
	
	public Evento guardarYactualizar(Evento evento);
	
	public void eliminar(Evento evento);

}
