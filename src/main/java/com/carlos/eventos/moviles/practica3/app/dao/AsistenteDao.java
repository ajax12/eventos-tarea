package com.carlos.eventos.moviles.practica3.app.dao;

import org.springframework.data.repository.CrudRepository;

import com.carlos.eventos.moviles.practica3.app.entity.Asistente;

public interface AsistenteDao extends CrudRepository<Asistente, Long> {

}
