package com.carlos.eventos.moviles.practica3.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EventosMovilesPractica3Application {

	public static void main(String[] args) {
		SpringApplication.run(EventosMovilesPractica3Application.class, args);
	}

}
