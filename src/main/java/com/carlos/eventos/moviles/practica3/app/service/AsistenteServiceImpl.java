package com.carlos.eventos.moviles.practica3.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.carlos.eventos.moviles.practica3.app.dao.AsistenteDao;
import com.carlos.eventos.moviles.practica3.app.entity.Asistente;

@Service
public class AsistenteServiceImpl implements AsistenteService{
	
	@Autowired
	AsistenteDao asistenteDao;

	@Override
	@Transactional(readOnly = true)
	public List<Asistente> listarAsistentes() {
		
		return (List<Asistente>) asistenteDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Asistente buscarAsistentePorId(Long id) {
		
		return asistenteDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Asistente guardarYactualizarAsistente(Asistente asistente) {
		
		return asistenteDao.save(asistente);
	}

	@Override
	@Transactional
	public void eliminarAsistente(Asistente asistente) {
		asistenteDao.delete(asistente);
		
	}
	

}
