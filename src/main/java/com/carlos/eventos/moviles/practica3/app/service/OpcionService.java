package com.carlos.eventos.moviles.practica3.app.service;

import java.util.List;

import com.carlos.eventos.moviles.practica3.app.entity.Opcion;

public interface OpcionService {
	
public List<Opcion> listarOpciones();
	
	public Opcion buscarOpcionPorId(Long id);
	
	public Opcion guardarYactualizar(Opcion opcion);
	
	public void eliminar(Opcion opcion);
	


}
