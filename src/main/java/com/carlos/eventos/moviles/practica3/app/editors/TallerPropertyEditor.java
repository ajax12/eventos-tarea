package com.carlos.eventos.moviles.practica3.app.editors;

import java.beans.PropertyEditorSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carlos.eventos.moviles.practica3.app.service.TallerService;

@Component
public class TallerPropertyEditor extends PropertyEditorSupport {

	@Autowired
	private TallerService service;
	
	@Override
	public void setAsText(String idString) throws IllegalArgumentException {
		if(idString != null && idString.length() > 0) {
			try {
				Integer id = Integer.parseInt(idString);
				this.setValue(service.buscarTallerPorId(id));
				
			} catch (NumberFormatException e) {
				setValue(null);
				
			}
		}else {
				setValue(null);
			}
			
		}
	}
	
	


