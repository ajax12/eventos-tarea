package com.carlos.eventos.moviles.practica3.app.dao;

import org.springframework.data.repository.CrudRepository;

import com.carlos.eventos.moviles.practica3.app.entity.Taller;

public interface TallerDao extends CrudRepository<Taller, Integer>{

}
