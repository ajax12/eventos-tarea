package com.carlos.eventos.moviles.practica3.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.carlos.eventos.moviles.practica3.app.dao.OpcionDao;
import com.carlos.eventos.moviles.practica3.app.entity.Opcion;


@Service
public class OpcionServiceImpl implements OpcionService {

	@Autowired
	OpcionDao opcionDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Opcion> listarOpciones() {
		
		return (List<Opcion>) opcionDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Opcion buscarOpcionPorId(Long id) {
		// TODO Auto-generated method stub
		return opcionDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Opcion guardarYactualizar(Opcion opcion) {
		
		return opcionDao.save(opcion);
	}

	@Override
	@Transactional
	public void eliminar(Opcion opcion) {
		opcionDao.delete(opcion);
	}

}
