package com.carlos.eventos.moviles.practica3.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.carlos.eventos.moviles.practica3.app.dao.TallerDao;
import com.carlos.eventos.moviles.practica3.app.entity.Taller;

@Service
public class TallerServiceImpl implements TallerService{
	
	@Autowired
	TallerDao tallerDao;

	@Override
	@Transactional(readOnly = true)
	public List<Taller> listarTalleres() {
		
		return (List<Taller>) tallerDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Taller buscarTallerPorId(Integer id) {
		
		return tallerDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Taller guardarYactualizarTaller(Taller taller) {
		
		return tallerDao.save(taller);
	}

	@Override
	@Transactional
	public void eliminarTaller(Taller taller) {
		tallerDao.delete(taller);
		
	}
	

}
