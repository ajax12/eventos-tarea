package com.carlos.eventos.moviles.practica3.app.controllers;



import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;

import com.carlos.eventos.moviles.practica3.app.entity.Evento;
import com.carlos.eventos.moviles.practica3.app.service.EventoService;


@Controller
@SessionAttributes("evento")
public class EventoController {
	

	@Autowired
	EventoService service;
	
	@GetMapping(value = {"/","/listado"})
	public String listaEventos(Model model) {
		List<Evento> eventos = service.listarEventos();
		model.addAttribute("eventos", eventos);
		model.addAttribute("titulo", "Proximos Eventos");
		
		return "listado";
		
	}
	
	@GetMapping(value = "/listar")
	public String listar(Model model) {
		List<Evento> eventos = service.listarEventos();
		model.addAttribute("eventos", eventos);
		model.addAttribute("titulo", "administracion de eventos");
		return "/eventos/listar-eventos";
		
	}
	
	@GetMapping(value = "/formulario-evento")
	public String crearEvento(Model model) {
		Evento evento = new Evento();
		model.addAttribute("evento", evento);
		model.addAttribute("msg", "Crear nuevo evento");
		return "/eventos/form-registro-evento";
	}
	
	@PostMapping(value = "/formulario-evento")
	public String guardarEvento(Evento evento,@RequestParam("file") MultipartFile imagen, SessionStatus status) {
		
		if(!imagen.isEmpty()) {
			
			String raiz = "/home/ajax/Escritorio/uploads";
			
			try {
				byte[] bytes = imagen.getBytes();
				Path rutaCompleta = Paths.get(raiz + "//" + imagen.getOriginalFilename());
				Files.write(rutaCompleta, bytes);
				evento.setImagen(imagen.getOriginalFilename());
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		
		service.guardarYactualizar(evento);
		status.setComplete();
		return "redirect:/listar";
	}
	
	@GetMapping(value = "/formulario-evento/{id}")
	public String editarEvento(@PathVariable(name = "id") Long id,Model model) {
		Evento evento = service.buscarEventoPorId(id);
		model.addAttribute("evento", evento);
		model.addAttribute("msg", "Editar Evento");
		
		return "/eventos/form-registro-evento";
	}
	
	@GetMapping(value = "/eliminar-evento/{id}")
	public String eliminarEvento(@PathVariable(name = "id") Long id) {
		Evento evento = service.buscarEventoPorId(id);
		service.eliminar(evento);
		return "redirect:/listar";
	}
	
}
