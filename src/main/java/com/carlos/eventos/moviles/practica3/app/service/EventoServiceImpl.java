package com.carlos.eventos.moviles.practica3.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.carlos.eventos.moviles.practica3.app.dao.EventoDao;
import com.carlos.eventos.moviles.practica3.app.entity.Evento;

@Service
public class EventoServiceImpl implements EventoService{
	
	@Autowired
	EventoDao dao;

	@Override
	@Transactional(readOnly = true)
	public List<Evento> listarEventos() {
		
		return (List<Evento>) dao.findAll();
	}

	
	@Override
	@Transactional(readOnly = true)
	public Evento buscarEventoPorId(Long id) {
		
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Evento guardarYactualizar(Evento evento) {
		
		return dao.save(evento);
	}

	@Override
	@Transactional
	public void eliminar(Evento evento) {
		
		dao.delete(evento);
		
	}

}
