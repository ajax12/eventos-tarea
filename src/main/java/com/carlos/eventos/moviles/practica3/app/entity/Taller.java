package com.carlos.eventos.moviles.practica3.app.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;


@Entity
@Table(name = "talleres" )
public class Taller {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "inicio")
	private Date inicio;
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "termino")
	private Date termino;
	@Column(name = "nombre_taller")
	private String nombreTaller;
	@Column(name = "descripcion_taller")
	private String descripcionTaller;
	@Column(name = "impartido")
	private String impartido;
	@Column(name = "costo_taller")
	private Double costoTaller;
	
	
	
	

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Double getCostoTaller() {
		return costoTaller;
	}
	public void setCostoTaller(Double costoTaller) {
		this.costoTaller = costoTaller;
	}
	
	
	public Date getInicio() {
		return inicio;
	}
	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}
	public Date getTermino() {
		return termino;
	}
	public void setTermino(Date termino) {
		this.termino = termino;
	}
	public String getNombreTaller() {
		return nombreTaller;
	}
	public void setNombreTaller(String nombreTaller) {
		this.nombreTaller = nombreTaller;
	}
	public String getDescripcionTaller() {
		return descripcionTaller;
	}
	public void setDescripcionTaller(String descripcionTaller) {
		this.descripcionTaller = descripcionTaller;
	}
	public String getImpartido() {
		return impartido;
	}
	public void setImpartido(String impartido) {
		this.impartido = impartido;
	}
	
}
