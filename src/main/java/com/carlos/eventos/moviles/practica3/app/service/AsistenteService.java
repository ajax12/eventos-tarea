package com.carlos.eventos.moviles.practica3.app.service;

import java.util.List;

import com.carlos.eventos.moviles.practica3.app.entity.Asistente;

public interface AsistenteService {
	
    public List<Asistente> listarAsistentes();
	
	public Asistente buscarAsistentePorId(Long id);
	
	public Asistente guardarYactualizarAsistente(Asistente asistente);
	
	public void eliminarAsistente(Asistente asistente);
	
	

}
