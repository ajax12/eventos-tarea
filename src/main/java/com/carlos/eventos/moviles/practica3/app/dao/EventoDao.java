package com.carlos.eventos.moviles.practica3.app.dao;

import org.springframework.data.repository.CrudRepository;

import com.carlos.eventos.moviles.practica3.app.entity.Evento;

public interface EventoDao extends CrudRepository<Evento, Long>{

}
