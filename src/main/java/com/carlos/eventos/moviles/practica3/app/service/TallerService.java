package com.carlos.eventos.moviles.practica3.app.service;

import java.util.List;

import com.carlos.eventos.moviles.practica3.app.entity.Taller;

public interface TallerService {
	
    public List<Taller> listarTalleres();
	
	public Taller buscarTallerPorId(Integer id);
	
	public Taller guardarYactualizarTaller(Taller taller);
	
	public void eliminarTaller(Taller taller);
	
	

}
