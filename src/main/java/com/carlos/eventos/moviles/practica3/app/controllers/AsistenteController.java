package com.carlos.eventos.moviles.practica3.app.controllers;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.carlos.eventos.moviles.practica3.app.editors.TallerPropertyEditor;
import com.carlos.eventos.moviles.practica3.app.entity.Asistente;
import com.carlos.eventos.moviles.practica3.app.entity.Evento;
import com.carlos.eventos.moviles.practica3.app.entity.Opcion;
import com.carlos.eventos.moviles.practica3.app.entity.Taller;
import com.carlos.eventos.moviles.practica3.app.service.AsistenteService;
import com.carlos.eventos.moviles.practica3.app.service.EventoService;
import com.carlos.eventos.moviles.practica3.app.service.OpcionService;
import com.carlos.eventos.moviles.practica3.app.service.TallerService;



@Controller
@SessionAttributes({"asistente","evento","opcion"})

@RequestMapping("/registros")
public class AsistenteController {

	@Autowired
	AsistenteService asistenteService;

	@Autowired
	EventoService eventoService;
	
	@Autowired
	OpcionService opcionService;
	
	
	
	@Autowired
	TallerService tallerService;
	
	@Autowired
	TallerPropertyEditor editors;
	
	@InitBinder
	public void intBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Taller.class,"taller",editors);
	}
	
	@GetMapping(value = "/listar")
	public String listaAsistentes(Model model) {
		
		List<Opcion> opciones = opcionService.listarOpciones();
		
		List<Asistente> asistentes = asistenteService.listarAsistentes();
		model.addAttribute("asistentes", asistentes);
		
		
		Double total = 0.0;
		for(Opcion opcion: opciones)
			total += opcion.getCostoEvento();
		
		model.addAttribute("total", total);
		
		
		
		model.addAttribute("titulo", "Asistentes confirmados");

		return "/asistentes/listar-asistentes";

	}

	@GetMapping(value = "/form-registro/{idEvento}")
	public String crearRegistros(@PathVariable(name = "idEvento") Long idEvento, Model model) {

		Evento evento = eventoService.buscarEventoPorId(idEvento);
		Asistente asistente = new Asistente();
		asistente.setEvento(evento);
		model.addAttribute("asistente", asistente);
		model.addAttribute("msg", "Registrando a evento " + asistente.getEvento().getTitulo());
		
		return "/asistentes/form-registro";
	}
	

	@PostMapping(value = "/form-registro")
	public String guardarAsistente(Asistente asistente,Model model) throws ParseException {
		asistente.getOpcion().getActualizaTotales();
		opcionService.guardarYactualizar(asistente.getOpcion());
		
		asistenteService.guardarYactualizarAsistente(asistente);
	
		return "/asistentes/gafete";
	}
	
	@GetMapping(value = "/form-registros/{id}")
	public String editarAsistente(@PathVariable("id") Long id, Model model) {
		
		Asistente asistente = asistenteService.buscarAsistentePorId(id);
		
		model.addAttribute("asistente", asistente);
		model.addAttribute("msg", "Editar Registro");
		
		return "/asistentes/form-editar";
	}
	
	


	@GetMapping(value = "/eliminar-registro/{id}")
	public String eliminarAsistente(@PathVariable(name = "id") Long id) {
		Asistente asistente = asistenteService.buscarAsistentePorId(id);
		asistenteService.eliminarAsistente(asistente);
		return "redirect:/registros/listar";
	}
	
	@GetMapping("/gafete")
	public String verGafete(Asistente asistente, Model model,SessionStatus status) {
		
		status.setComplete();
		return "/asistentes/gafete";
	}
	
@ModelAttribute("listaTalleres")
public List<Taller> listaTaller(){		
	return tallerService.listarTalleres();
		}
	
	
	
	
}
