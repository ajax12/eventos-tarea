package com.carlos.eventos.moviles.practica3.app.entity;



import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "opciones")
public class Opcion {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "comida")
	private boolean comida;
	@Column(name="costo_evento")
	private Double costoEvento;
	
	private Double costoComida;
	
	private Double costoTaller;
	
	private Double total;
	
	
	
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fechaDeCompra;
	
	

	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "taller_id")
	private Taller taller;
	
	
	
	
	
	public Opcion() {
		
		this.costoEvento = 500.00;
	}

	public Date getFechaDeCompra() {
		return fechaDeCompra;
	}
	
	public void setFechaDeCompra(Date fechaDeCompra) {
		this.fechaDeCompra = fechaDeCompra;
	}
	
	
	public boolean isComida() {
		return comida;
	}

	public void setComida(boolean comida) {
		this.comida = comida;
	}

	
	public Double getCostoEvento() {
		return costoEvento;
	}

	public void setCostoEvento(Double costoEvento) {
		this.costoEvento = costoEvento;
	}

	public Taller getTaller() {
		return taller;
	}

	public void setTaller(Taller taller) {
		this.taller = taller;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public void asignarCostoComida() {
		
		if(comida == true) {
			costoComida = 200.00;
			
		}
		else {
			costoComida = 0.0;
			
		}
		
		
	}
	
	
	
	public Double getCostoComida() {
		return costoComida;
	}

	public void setCostoComida(Double costoComida) {
		this.costoComida = costoComida;
	}

	public Double getCostoTaller() {
		return costoTaller;
	}

	public void setCostoTaller(Double costoTaller) {
		this.costoTaller = costoTaller;
	}
	
	
	

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}
	
	public double totalFactura() {
		return costoTaller + costoComida + costoEvento;
	}

	public void getActualizaTotales() throws ParseException {
		String fechaPreventa = "2021/09/01";
		SimpleDateFormat formatEntrada = new SimpleDateFormat("yyyy/MM/dd");
		Date fecha = formatEntrada.parse(fechaPreventa);

		if(fecha.compareTo(fechaDeCompra) != 1){
		    setCostoEvento(800.00);
		    setCostoTaller(500.00);
		    
		    
		    asignarCostoComida();
		    setTotal(totalFactura());
		      
		}else {
			setCostoTaller(getTaller().getCostoTaller());
			
			    setCostoEvento(500.00);
			    asignarCostoComida();
			    setTotal(totalFactura());
			
		}
		
		
	}

	
	
	
	
	
	
	
	
	
	
}
